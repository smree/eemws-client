# IEC 62325-504 client #

### What is this repository for? ###

This is (was) a client implementation of IEC 62325-504 technical specification.

* **eemws-client** includes client classes to invoke the eem web services
* **eemws-kit** includes command line utilities to invoke the eem web services, as well as several GUI applications (browser, editor, ...).

**This repository, including its history, has been merged with the [eemws-core](https://bitbucket.org/smree/eemws-core) repository.**

Please refer to [eemws-core](https://bitbucket.org/smree/eemws-core/).
